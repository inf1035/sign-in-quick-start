﻿namespace SigninQuickstart
{
    using Android.App;
    using Android.Content;
    using Android.Gms.Auth.Api;
    using Android.Gms.Auth.Api.SignIn;
    using Android.Gms.Common;
    using Android.Gms.Common.Apis;
    using Android.Gms.Drive;
    using Android.OS;
    using Android.Support.V7.App;
    using Android.Views;

    [Activity(MainLauncher = true, Theme = "@style/ThemeOverlay.MyNoTitleActivity")]
    public class MainActivity : AppCompatActivity,
                                View.IOnClickListener,
                                GoogleApiClient.IConnectionCallbacks,
                                GoogleApiClient.IOnConnectionFailedListener
    {
        private const int RC_SIGN_IN = 9001;

        private string idToken;

        private GoogleApiClient mGoogleApiClient;

        public void OnClick(View v)
        {
            switch (v.Id)
            {
                case Resource.Id.sign_in_button:
                    Intent signInIntent = Auth.GoogleSignInApi.GetSignInIntent(this.mGoogleApiClient);
                    this.StartActivityForResult(signInIntent, RC_SIGN_IN);
                    break;
            }
        }

        public void OnConnected(Bundle connectionHint)
        {
            Auth.GoogleSignInApi.SignOut(this.mGoogleApiClient);
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            if (result.HasResolution)
            {
                try
                {
                    result.StartResolutionForResult(this, RC_SIGN_IN);
                }
                catch (IntentSender.SendIntentException e)
                {
                    this.mGoogleApiClient.Connect();
                }
            }
        }

        public void OnConnectionSuspended(int cause)
        {
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == RC_SIGN_IN)
            {
                GoogleSignInResult result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);

                if (result.IsSuccess)
                {
                    this.idToken = result.SignInAccount.ServerAuthCode;
                    this.mGoogleApiClient.Reconnect();
                }
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.SetContentView(Resource.Layout.activity_main);

            this.FindViewById(Resource.Id.sign_in_button).SetOnClickListener(this);
            this.FindViewById<SignInButton>(Resource.Id.sign_in_button).SetSize(SignInButton.SizeWide);

            GoogleSignInOptions gso =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn).RequestScopes(new Scope("https://www.googleapis.com/auth/drive"))
                    .RequestEmail()
                    .RequestServerAuthCode("1068370088447-vafcdctam5igp70pm5q89iqm3t5co3ja.apps.googleusercontent.com")
                    .Build();

            this.mGoogleApiClient =
                new GoogleApiClient.Builder(this).AddConnectionCallbacks(this)
                    .AddOnConnectionFailedListener(this)
                    .AddApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .Build();
            this.mGoogleApiClient.Connect();
        }
    }
}